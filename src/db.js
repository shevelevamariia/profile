import firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCxscfyWWu9lkU1K9UCpguovkIdgf_EA4E",
  authDomain: "test-ee813.firebaseapp.com",
  databaseURL: "https://test-ee813.firebaseio.com",
  projectId: "test-ee813",
  storageBucket: "test-ee813.appspot.com",
  messagingSenderId: "449458710999",
  appId: "1:449458710999:web:3e3cbffb6e44a978858392"
};

export const db = firebase.initializeApp(firebaseConfig).firestore();

const { Timestamp, GeoPoint } = firebase.firestore;
export { Timestamp, GeoPoint };

db.settings({});
